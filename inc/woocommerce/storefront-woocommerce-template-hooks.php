<?php
/**
 * Storefront WooCommerce hooks
 *
 * @package storefront
 */


/**
 * Header
 *
 * @see storefront_product_search()
 * @see storefront_header_cart()
 */

add_action( 'after_setup_theme', 'storefront_child_lg_woocommerce_header_hooks' );
function storefront_child_lg_woocommerce_header_hooks() {
    remove_action( 'storefront_header',             'storefront_header_cart',   60 );
    add_action( 'storefront_header',                'storefront_header_cart',   25 );
    remove_action( 'storefront_before_content',     'woocommerce_breadcrumb',   10 );
}
